﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderMenu : MonoBehaviour
{
    public Slider slider = null;
    public AudioSource audioSource = null;
    private void Start()
    {
        slider.onValueChanged.AddListener(delegate { sliderChange(); });
    }
    void sliderChange()
    {
        audioSource.volume = slider.value * slider.value;
    }
}
