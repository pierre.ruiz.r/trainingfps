﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    //public Rigidbody rb;
    public GameObject f_camera;
    public GameObject player;
    public float rot_vel;
    Vector3 currentAngle;
    void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {
        //Cursor.lockState = CursorLockMode.Locked;
    }
    void FixedUpdate()
    {
        float turn_x = Input.GetAxis("Mouse X");
        currentAngle += new Vector3(0, turn_x, 0)*rot_vel;
        f_camera.transform.eulerAngles = currentAngle;
        player.transform.eulerAngles = currentAngle;
        transform.eulerAngles = currentAngle;
    }
}
