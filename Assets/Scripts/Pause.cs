﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    public Button resume = null;
    public Button exit = null;
    public GameObject pauseMenu = null;

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        resume.onClick.AddListener(() =>
        {
            Time.timeScale = 1;
            resume.gameObject.SetActive(false);
            exit.gameObject.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        });
        exit.onClick.AddListener(() =>
        {
            Application.Quit();
        });
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if(Time.timeScale == 1.0f)
            {
                Time.timeScale = 0;
                resume.gameObject.SetActive(true);
                exit.gameObject.SetActive(true);
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Time.timeScale = 1;
                resume.gameObject.SetActive(false);
                exit.gameObject.SetActive(false);
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }
}
