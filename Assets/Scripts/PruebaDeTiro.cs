﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaDeTiro : MonoBehaviour
{

    public float tiempoEspera=2,limitx=5,limity=5;
    float tiempo;
    int score = 0;
    Vector3 PositionObjectivo,posInicial;
    public int cambios,asertados;
    [SerializeField] bool isTimerWorking;
    // Start is called before the first frame update
    void Start()
    {
        asertados = 0;
        cambios = 0;
        posInicial = gameObject.transform.position;
        PositionObjectivo = posInicial+new Vector3(Random.Range(-limitx,limitx), Random.Range(-limity,limity),0);
        gameObject.transform.position = PositionObjectivo;
        tiempo = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isTimerWorking) return; 
        if (Time.time>=tiempo+tiempoEspera)
        {
            PositionObjectivo = posInicial + new Vector3(Random.Range(-limitx, limitx), Random.Range(-limity, limity), 0);
            gameObject.transform.position = PositionObjectivo;
            tiempo = Time.time;
            cambios += 1;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("impactado");
        if (collision.gameObject.GetComponent<Bullet>()!=null)
        {
            asertados += 1;
            cambios += 1;
            PositionObjectivo = posInicial + new Vector3(Random.Range(-limitx, limitx), Random.Range(-limity, limity), 0);
            gameObject.transform.position = PositionObjectivo;
            tiempo = Time.time;
            Debug.Log("impactado");
            Debug.Log("Score : "+score);
        }
    }
}
