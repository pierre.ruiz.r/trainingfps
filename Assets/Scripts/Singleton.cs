﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour
{
    private static Singleton single;

    private void Awake()
    {
        if (single != null && single != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            single = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
