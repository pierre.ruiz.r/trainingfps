﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Rigidbody BulletRB;
    public MeshRenderer BulletRenderer;
    public Collider BulletCollider;
    public float Duration = 1;
    float timeSpawn=0;
    public bool enable;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (enable==true &&Time.time>=Duration+timeSpawn) {
            DesactiveBullet();
        }

    }
    public void ActiveBullet()
    {
        gameObject.SetActive(true);
        enable = true;
        /*
        BulletRB.velocity = Vector3.zero;
        BulletRB.useGravity = true;
        BulletRenderer.enabled = true;
        BulletCollider.isTrigger = false;*/
        timeSpawn = Time.time;

    }
    public void DesactiveBullet()
    {
        gameObject.SetActive(false);
        enable = false;
        BulletRB.velocity = Vector3.zero;
        /*BulletCollider.isTrigger = true;
        BulletRB.useGravity = false;
        BulletRenderer.enabled = false;
        BulletRB.velocity = Vector3.zero;*/
    }
   
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("chocó con  "+collision.transform.name); 
        DesactiveBullet();
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("chocó (trigger) con  " + other.transform.name);
        DesactiveBullet();
    }
}
