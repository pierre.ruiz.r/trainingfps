﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shot : MonoBehaviour
{
    public float force = 10,timeActiveBullet=0.5f;
    public int cargador = 20,maxBullets=10;
    public GameObject pointShot;
    public GameObject bulletObject;
    List<GameObject> bullets = new List<GameObject>();
    //public GameObject shotingCenter;
    float shottTime, startRecharge,startd;
    int BulletFired = 0;
    public bool automatic;
    // Start is called before the first frame update
    void Start()
    {
        BulletFired = 0;
        for (int i = 0; i < maxBullets; i++)
        {
            bullets.Add(GameObject.Instantiate(bulletObject, pointShot.transform.position, pointShot.transform.rotation));
            reacomodoBullet(bullets[i]);
            bullets[i].transform.parent = pointShot.transform;
        }
        shottTime = Time.time;
        startd = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (automatic==true)
        {
            if (Input.GetButton("Fire1") && Time.time >= shottTime + timeActiveBullet )
            {
                //StartCoroutine(ShottingRate(timeActive));

                if (BulletFired < cargador)
                {
                    GameObject b = BulletReady();
                    shotBullet(b);
                    shottTime = Time.time;
                }
            }
        }
        else
        {
            if (Input.GetButtonDown("Fire1") && Time.time >= shottTime + timeActiveBullet)
            {
                //StartCoroutine(ShottingRate(timeActive));

                    GameObject b = BulletReady();
                    shotBullet(b);
                    shottTime = Time.time;
                
            }
        }

        /*if (Input.GetButtonDown("Fire1"))
        {
            Debug.Log( Time.time - startd);

            startd = Time.time;
        }*/

       
    }
   

   public void shotBullet(GameObject b)
    {
        b.GetComponent<Bullet>().ActiveBullet();
        b.transform.parent = null;

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward,out hit, 1000))
        {
            b.GetComponent<Rigidbody>().AddForce(Vector3.Normalize(hit.point-pointShot.transform.position)* force, ForceMode.Impulse);
        }
        else
        {
            b.GetComponent<Rigidbody>().AddForce(pointShot.transform.forward * force, ForceMode.Impulse);
        }
        
    }
    public GameObject BulletReady(){

        for (int i = 0; i < maxBullets; i++)
        {
            if (bullets[i].GetComponent<Bullet>().enable == false)
            {
                reacomodoBullet(bullets[i]);
                return bullets[i];
            }
            if (i==maxBullets-1&& bullets[maxBullets-1].GetComponent<Bullet>().enabled==true)
            {
                bullets.Add(GameObject.Instantiate(bulletObject, pointShot.transform.position, pointShot.transform.rotation));
                maxBullets += 1;
                reacomodoBullet(bullets[i+1]);
                bullets[i].transform.parent = pointShot.transform;
            }
        }
        
        return null;
    }
    public void reacomodoBullet(GameObject b)
    {
        b.transform.parent = pointShot.transform;
        b.GetComponent<Bullet>().DesactiveBullet();
        b.transform.forward = pointShot.transform.forward;
        b.transform.position = pointShot.transform.position;
        b.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
    IEnumerator ShottingRate(float t){
       
        //if (Input.GetButtonDown("Fire1")){

            
            if (BulletFired < cargador)
            {

                GameObject b = BulletReady();
                shotBullet(b);
            }
          yield return new WaitForSeconds(t);
        //}  

    }
}
