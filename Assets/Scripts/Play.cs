﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Play : MonoBehaviour
{
    public Button start=null;
    public Button exit=null;
    public InputField playerName=null;
    public Option options = null;
    public string player = "Player";
    private void Start()
    {
        start.onClick.AddListener(() => {
            SceneManager.LoadScene(1);
            options.player = playerName.text;
        });
        exit.onClick.AddListener(() =>
        {
            Application.Quit();
        });
    }
}
