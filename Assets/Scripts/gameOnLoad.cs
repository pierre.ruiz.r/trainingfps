﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameOnLoad : MonoBehaviour
{
    public Text playerName = null;
    Option options = null;
    private void Start()
    {
        options = GameObject.Find("Singleton").GetComponent<Option>();
        playerName.text = options.player;
    }
}
