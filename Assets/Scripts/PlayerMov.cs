﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMov : MonoBehaviour
{
    [Header("Movement")]
    public float speed = 0f;
    public float groundDrag = 0f;
    [Header("Jump")]
    public float jumpForce = 0f;
    public float multiplierAirSpeed = 0f;
    public float airDrag = 0f;
    public bool grounded = false;

    private Rigidbody rb = null;
    private Animator anim = null;
    private float horizontalMov = 0f;
    private float verticalMov = 0f;
    private Vector3 movDir = Vector3.zero;
    private AudioSource audioSource = null;
    public AudioClip audioClip = null;
    private bool mov_LR;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        mov_LR = false;
        //Vel = 0
        if(Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            mov_LR = true;
            anim.SetBool("walk", false);
            //Debug.Log(rb.velocity);
        }
        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
        {
            rb.velocity = new Vector3(this.rb.velocity.x * transform.forward.x, this.rb.velocity.y * transform.forward.y, 0);
            anim.SetBool("walk", false);
            //Debug.Log(rb.velocity);
        }
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            anim.SetBool("walk", true);
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            anim.SetBool("shoot", true);
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            anim.SetBool("shoot", false);
        }
        horizontalMov = Input.GetAxisRaw("Horizontal");
        verticalMov = Input.GetAxisRaw("Vertical");
        anim.SetFloat("ForBack", verticalMov);
        anim.SetFloat("RigLef", horizontalMov);
        //fix
        movDir = ((transform.forward * verticalMov) + (transform.right * horizontalMov)).normalized;

        if (grounded)
        {
            rb.drag = groundDrag;
        }
        else
        {
            rb.drag = airDrag;
            //Debug.Log(movDir * speed);
        }

        if (Input.GetKeyDown(KeyCode.Space) && grounded){
            rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
            grounded = false;
        }
    }

    private void FixedUpdate()
    {
        if (grounded)
        {
            rb.AddForce(movDir * speed, ForceMode.Force);
            //Debug.Log(movDir * speed);
        }
        else
        {
            rb.AddForce(movDir * speed * multiplierAirSpeed, ForceMode.Force);

            //Debug.Log(movDir * speed);
        }
        if (mov_LR)
        {
            rb.velocity = new Vector3(0, this.rb.velocity.y * transform.forward.y, this.rb.velocity.z * transform.forward.z);
            //mov_LR = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            grounded = true;
        }
    }
    void Step()
    {
        if (anim.GetBool("walk"))
        {
            audioSource.PlayOneShot(audioClip,0.2f*0.2f);
            Debug.Log("paso");
        }
    }
}
